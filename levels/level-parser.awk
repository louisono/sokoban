#! /usr/bin/env -S awk -f

BEGIN {
	PARSE_TABLE["X"] = "Wall"
	PARSE_TABLE["."] = "Floor"
	PARSE_TABLE["P"] = "Pusher"
	PARSE_TABLE["B"] = "Box"
	PARSE_TABLE["G"] = "Goal"
	NOTHING = "N o t h i n g b r u h"
	INDENT = "  "
	FS = " "
	row = 0
	row_max = 0
	col_max = 0
	layer = 0
}

function parse_char(single_char) {
	return (single_char in PARSE_TABLE) ? PARSE_TABLE[single_char] : NOTHING
}

function alength(arr) {
	count = 0
	for (_ in arr) count += 1
	return count
}

function join(arr, joiner) {
	len = alength(arr)
	joined = ""
	for (i=0; i<len-1; i++) {
		joined = joined arr[i] joiner
	}
	joined = joined arr[i]
	return joined
}

function make_stack(arr, i) {
	len = alength(arr)
	if (len == i+1)
		return "Bare " arr[i]
	else
		return "Covered (" arr[i] ", " arr[i+1] ")"
}

$0 !~ /^[ \t]*$/ {
	empty_line = 0
	# col starts at 1 cause $0 is the whole line
	for (col=1; col<=NF; col++) {
		if (col>col_max) col_max = col
		parsed = parse_char($col)
		if (parsed != NOTHING) {
			parsed_grid[row, (col-1), layer] = parsed
		}
	}
	row = row+1
	if (row>row_max) row_max = row
}

/^[ \t]*$/ {
	if (!empty_line) {
		empty_line = 1
		layer += 1
		row = 0
	}
}

END {
	print "{ height = " row_max "\n" INDENT ", width = " col_max "\n" INDENT ", content ="
	print INDENT INDENT "["
	for (row_num=0; row_num<row_max; row_num++) {
		delete current_row
		for (col_num=0; col_num<col_max; col_num++) {
			delete stack
			count_stack_elements = 0
			for (layer_num=0; layer_num<=layer; layer_num++) {
				if ((row_num, col_num, layer_num) in parsed_grid) {
					count_stack_elements += 1
					stack[layer_num] = parsed_grid[row_num, col_num, layer_num]
				}
			}
			stack_string = make_stack(stack, 0)
			current_row[col_num] = stack_string
		}
		row_string = "[" join(current_row, ", ") "]"
		current_indent = row_num == 0 ? INDENT INDENT INDENT : INDENT INDENT INDENT ", "
		print current_indent row_string
	}
	print INDENT INDENT "]"
	print "}"
}
