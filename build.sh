#! /usr/bin/env bash
THIS_DIR="$(dirname $0)"
SRC="$THIS_DIR/src/Main.elm"
TARGET="$THIS_DIR/sokoban.js"

elm make $SRC --output=$TARGET
