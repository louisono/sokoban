-- we griding in this mf

module Grid exposing (..)


{- 2D Array, where all the rows have the same width.
-}
type alias Grid a = 
  { width: Int
  , height: Int
  , content: List (List a)
  }


{- Create a `Grid` by using given function of x y coordinates.
-}
initXY: Int -> Int -> (Int -> Int -> a) -> Grid a
initXY w h fxy =
  let
    auxinit i len fi =
      if i >= len then [] else (fi i)::(auxinit (i+1) len fi)
  in
  auxinit 0 h (\y -> auxinit 0 w (\x -> fxy x y))
  |> (\c -> { width = w, height = h, content = c })


{- Return cell from the given `Grid` at given coordinates if within bounds. 
Otherwise, `Nothing`.
-}
getXY: Int -> Int -> Grid a -> (Maybe a)
getXY x y { content } =
  let 
    nth n = List.head << List.drop n
  in
  nth y content
  |> Maybe.andThen (nth x)


{- Return updated `Grid` with cell at given coordinates replaced by given new 
`a` value.
-}
setXY: Int -> Int -> a -> Grid a -> Grid a
setXY x y newVal ({ content } as g) =
  let 
    updtEl x1 el = if x /= x1 then el else newVal
    updtRow y1 row = if y /= y1 then row else (List.indexedMap updtEl row)
  in
  List.indexedMap updtRow content
  |> (\c -> { g | content = c })


{- Use given function of coordinates to update values of given `Grid`.
-}
mapXY: (Int -> Int -> a -> b) -> Grid a -> Grid b
mapXY fxy ({ width, height, content } as g) =
  let
    updatedContent =
      List.indexedMap 
        (\y row -> 
          List.indexedMap 
            (\x el -> fxy x y el) 
            row)
        content
  in
  { width = width, height = height, content = updatedContent }


{- Return a `List` of coordinates where elements pass the given condition on
the given `Grid`.
-}
findXY: (a -> Bool) -> Grid a -> List (Int, Int)
findXY filter { content } =
  let
    auxrow x y row matchingIndices =
      case row of
        [] -> matchingIndices
        e::es -> 
          auxrow 
            (x+1)
            y
            es
            (if filter e then (x, y)::matchingIndices else matchingIndices)
    aux y rows matchingIndices =
      case rows of
        [] -> List.reverse matchingIndices
        r::rs -> 
          auxrow 0 y r matchingIndices
          |> aux (y+1) rs
  in
  aux 0 content []


{- Fold row by row, then for each row column by column.
-}
foldXY: (Int -> Int -> a -> b -> b) -> b -> Grid a -> b 
foldXY accumulatorXY init { content } =
  let first = (\(a, _) -> a)
  in
  List.foldl 
    (\row (acc, y) -> 
      List.foldl 
        (\el (rowAcc, x) -> ((accumulatorXY x y el rowAcc), x+1))
        (acc, 0)
        row
      |> (\(rowAcc, _) -> (rowAcc, y+1)))
    (init, 0) 
    content
  |> first


{- Turn given `List` into a `Grid`. Checks that width is consistent, `Nothing` is
returned if that's not the case.
-}
fromList: List (List a) -> Maybe (Grid a)
fromList l =
  let 
    height = List.length l
    widthL = List.map List.length l
    consistentWidth = 
      List.foldl 
        (\width prevWidth -> 
          case prevWidth of
            Nothing -> Just (width, width)
            Just (min, max) ->
              if width < min
              then Just (width, max)
              else if width > max
              then Just (min, width)
              else Just (min, max))
        Nothing 
        widthL
      |> Maybe.andThen (\(min, max) -> if min == max then Just min else Nothing)
  in
  Maybe.map (\w -> { height = height, width = w, content = l }) consistentWidth


{- String representation of given `Grid`
-}
toString: (a -> String) -> Grid a -> String
toString stringRepresenter { content } =
  List.map
    (\row -> List.map (\el -> stringRepresenter el) row |> String.join "")
    content
  |> String.join "\n"
