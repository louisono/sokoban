-- sokozin
-- zoko, kolo
-- kolonie


module Main exposing (..)


import Soko as S exposing (Sokoban, Direction(..))
import Html exposing (Html, div, text)
import Html.Events exposing (on)
import Html.Attributes exposing (tabindex, id, autofocus)
import Browser exposing (sandbox)
import Json.Decode as D
import Levels


{- Message type to signal user input and update sokoban level accordingly.
-}
type UserInput
  = GoLeft
  | GoRight
  | GoUp
  | GoDown
  | Undo


type alias Game = 
  { history: List Sokoban
  , current: Sokoban
  }


{- Turn given `String` representation of the key code (corresponds to `code`
attribute to a keyboard event, see MDN doc[1]) into a legal `UserInput`.
Unallowed codes result in `Nothing` being returned.
[1]:https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code
-}
parseInput: String -> Maybe UserInput
parseInput strKey =
  if strKey == "ArrowLeft"
  then Just GoLeft
  else if strKey == "ArrowRight"
  then Just GoRight
  else if strKey == "ArrowUp"
  then Just GoUp
  else if strKey == "ArrowDown"
  then Just GoDown
  else if strKey == "KeyW"
  then Just Undo
  else Nothing


{- Make a `Decoder` from given `Maybe` value, if `Nothing` is passed then the
resulting `Decoder` fails with provided message.
-}
failForNothing: String -> Maybe a -> D.Decoder a
failForNothing failMessage m =
  case m of
    Nothing -> D.fail failMessage
    Just v -> D.succeed v


{- `Decoder` to extract/parse `UserInput` out of a keyboard event. 
-}
decodeInput: D.Decoder UserInput
decodeInput =
  D.at ["code"] D.string
  |> D.map parseInput
  |> D.andThen (failForNothing "unrecognized input")


{- Update `Sokoban` level according to given `UserInput`.
-}
updateSokoban: UserInput -> Game -> Game
updateSokoban inputMsg ({history, current} as game) =
  let
    mayDirection =
      case inputMsg of
        GoLeft -> Just Left
        GoRight -> Just Right
        GoUp -> Just Up
        GoDown -> Just Down
        Undo -> Nothing
  in
  case mayDirection of
    Just dir -> 
      { history = current :: history
      , current = S.move dir current
      }
    Nothing ->
      case history of
        [] -> game
        previous :: rest -> { history = rest, current = previous }


{- View `Sokoban` level.
-}
viewSokoban: Sokoban -> Html UserInput
viewSokoban sokobanLevel =
  let
    sokoStr = S.toString sokobanLevel
    onArrows = on "keyup" decodeInput
    winner = S.winSokoban sokobanLevel
  in
  div 
    [ (tabindex 1), onArrows, id "sokoban", autofocus True ] 
    ((text sokoStr) :: (if winner then [text "goodjob, winner!"] else []))


main =
  sandbox 
    { init = { current = Levels.sokoL1, history = [] }
    , update = updateSokoban
    , view = (\{current} -> viewSokoban current)
    }
