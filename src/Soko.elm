-- we sokoban
-- soko, loco, we pushing
-- boxes in places


module Soko exposing (..)


import Grid as G exposing (Grid)


-- game types


{- Game entities for floor parts: Floor, Wall, Goal
and above the floor game entities: Pusher (player controlled character) or Box
-}


{- Floor level cells -}
type L1Cell
  = Floor
  | Goal
  | Wall


{- Cells about the floor level -}
type L2Cell 
  = Pusher
  | Box


{- Cells in sokoban context: either uncovered floor-type cell, or floor cell
covered by some other non-floor cell. -}
type ContextCell 
  = Bare L1Cell
  | Covered (L1Cell, L2Cell)


{- Direction type for player moves
-}
type Direction 
  = Left
  | Right
  | Up
  | Down


type alias Position = (Int, Int)


{- Game level in a specific state
-}
type alias Sokoban = Grid ContextCell


-- game functions


{- Make the `Pusher`s of the given `Sokoban` move in the given `Direction`.
-}
move: Direction -> Sokoban -> Sokoban
move dir soko =
  let
    pusherPositions = posOfPusher soko |> List.sortBy (orderByDirection dir)
    movesToMake = List.map (\p -> (p, (shiftPos dir p))) pusherPositions
    makeMove from to moveSoko =
      let destinationCell = cellAtPos to moveSoko in
      if (Maybe.withDefault False << Maybe.map walkable) destinationCell
      then walk from to moveSoko
      else if (Maybe.withDefault False << Maybe.map pushable) destinationCell
      then push from dir moveSoko
      else moveSoko
  in
  List.foldl 
    (\(from, to) updatedSoko -> makeMove from to updatedSoko)
    soko
    movesToMake


{- Return the `List Position` where all the `Pusher`s of the given `Sokoban` are.
-}
posOfPusher: Sokoban -> List Position
posOfPusher soko =
  let
    isPusher ccell =
      case ccell of
        Covered (_, Pusher) -> True
        _ -> False
  in
  G.findXY isPusher soko


{- Make given `Position` comparable, in regards to given `Direction`.
-}
orderByDirection: Direction -> Position -> Int
orderByDirection dir (x, y) =
  case dir of
    Left -> x
    Right -> -x
    Up -> y
    Down -> -y


{- Return `True` if the given `ContextCell` can be walked on.
-}
walkable: ContextCell -> Bool
walkable ccell =
  case ccell of
    Bare Floor -> True
    Bare Goal -> True
    _ -> False


{- Return `True` if the given `ContextCell` can be pushed.
-}
pushable: ContextCell -> Bool
pushable ccell =
  case ccell of
    Covered (Floor, Box) -> True
    Covered (Goal, Box) -> True
    _ -> False


{- Walk `Pusher` at given `Position` to given target `Position` on given `Sokoban`.
-}
walk: Position -> Position -> Sokoban -> Sokoban
walk fromPos toPos =
  let
    changeP x y cellAtXY =
      if (x, y) == fromPos
      then removePusher cellAtXY
      else if (x, y) == toPos
      then addPusher cellAtXY
      else cellAtXY
  in
  G.mapXY changeP


{- Make the `Pusher` at given `Position` push the nearby cell in given `Sokoban`.
-}
push: Position -> Direction -> Sokoban -> Sokoban
push pusherPos dir soko =
  let
    boxPos = shiftPos dir pusherPos
    pushedPos = shiftPos dir boxPos
    mayPush =
      (\(x, y) -> G.getXY x y soko) pushedPos
      |> Maybe.map walkable
      |> Maybe.withDefault False
    changeP x y cellAtXY =
      if (x, y) == pusherPos
      then removePusher cellAtXY
      else if (x, y) == boxPos
      then l2cellReplace Box Pusher cellAtXY
      else if (x, y) == pushedPos
      then addBox cellAtXY
      else cellAtXY
  in
  if mayPush
  then G.mapXY changeP soko
  else soko


{- Remove a `Pusher` from given `Covered` cell.
-}
removePusher: ContextCell -> ContextCell
removePusher ccell =
  case ccell of
    Covered (floor, Pusher) -> Bare floor
    _ -> ccell


{- Add a `Pusher` only if the given `ContextCell` is a `Bare Floor`.
-}
addPusher: ContextCell -> ContextCell
addPusher ccell =
  case ccell of
    Bare Floor -> Covered (Floor, Pusher)
    Bare Goal -> Covered (Goal, Pusher)
    _ -> ccell


{- Add a `Box` only if the given `ContextCell` is a `Bare Floor`.
-}
addBox: ContextCell -> ContextCell
addBox ccell =
  case ccell of
    Bare Floor -> Covered (Floor, Box)
    Bare Goal -> Covered (Goal, Box)
    _ -> ccell


{- Replace given `L2Cell` by second given `L2Cell` in `ContextCell`.
-}
l2cellReplace: L2Cell -> L2Cell -> ContextCell -> ContextCell
l2cellReplace replacedCell replacerCell ccell =
  case ccell of
    Covered (floor, covering) ->
      if covering == replacedCell 
      then Covered (floor, replacerCell) 
      else ccell
    _ -> ccell


{- Return `False` if the given `ContextCell` consists of a `Goal` that is covered
by a `Box`, `True` otherwise.
-}
winCell: ContextCell -> Bool
winCell ccell =
  case ccell of
    Covered (Goal, Box) -> True
    Covered (Goal, _) -> False
    Bare Goal -> False
    _ -> True


{- Return `True` if the given `Sokoban` has been completed, that is: every
`Goal` is covered by a `Box`.
-}
winSokoban: Sokoban -> Bool
winSokoban =
  G.foldXY (\_ _ cell acc -> acc && (winCell cell)) True


{- Move given `Position` in given `Direction`.
-}
shiftPos: Direction -> Position -> Position
shiftPos dir (x, y) =
  case dir of
    Left -> (x-1, y)
    Right -> (x+1, y)
    Up -> (x, y-1)
    Down -> (x, y+1)


{- Return the `ContextCell` from given `Sokoban` at given `Position`.
-}
cellAtPos: Position -> Sokoban -> Maybe ContextCell
cellAtPos (x, y) =
  G.getXY x y


{- Return `String` representation of given `ContextCell`.
-}
cellToString: ContextCell -> String
cellToString c =
  case c of
    Bare Floor -> "."
    Bare Goal -> "*"
    Bare Wall -> "X"
    Covered (_, Pusher) -> "@"
    Covered (_, Box) -> "O"


{- Return `String` representation of given `Sokoban`.
-}
toString: Sokoban -> String
toString =
  G.toString cellToString
