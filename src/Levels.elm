-- just the levels
-- just the levels
-- just the levels
-- just the levels
-- just the levels
-- ...  - louisono :)

module Levels exposing (..)


import Soko exposing (ContextCell(..), L1Cell(..), L2Cell(..))

sokoL1 = 
  { height = 9
  , width = 11
  , content =
    [ [Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall]
    , [Bare Wall, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Wall, Bare Floor, Bare Wall, Bare Wall]
    , [Bare Wall, Bare Floor, Covered (Floor, Pusher), Bare Floor, Bare Floor, Bare Floor, Covered (Floor, Pusher), Bare Floor, Bare Floor, Bare Floor, Bare Wall]
    , [Bare Wall, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Wall, Bare Goal, Bare Wall, Bare Wall]
    , [Bare Wall, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Wall, Bare Wall, Bare Wall, Bare Wall]
    , [Bare Wall, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Wall, Bare Wall, Bare Wall, Bare Wall]
    , [Bare Wall, Bare Floor, Covered (Floor, Box), Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Wall, Bare Wall, Bare Wall, Bare Wall]
    , [Bare Wall, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Floor, Bare Wall, Bare Wall, Bare Wall, Bare Wall]
    , [Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall, Bare Wall]
    ]
  }
