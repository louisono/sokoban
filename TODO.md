# Just a list of things that might be nice to have

- collection of levels
- evil twin (Pusher that moves in opposite direction)
- allow custom key-binding
- make it look nice(r)
- mobile controls?
- some actual levels
- better grid (Array?)
- pits?
- level editor?

# done :)

- autofocus
- undo 
- simpler stacks! just need (floor | floor & moving element)
